bridge-method-injector (1.25-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.25
  * Remove java9-compatibility patch
  * Patch for io.jenkins.tools.incrementals:git-changelist-maven-extension
  * Add build-dep on libfindbugs-annotations-java and adjust coordinates
  * Ignore parent pom
  * Adjust debian/maven.rules for junit
  * Ignore org.jenkins-ci:jenkins
  * Set maven.compiler.release=8

 -- tony mancill <tmancill@debian.org>  Thu, 17 Nov 2022 21:02:10 -0800

bridge-method-injector (1.23-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.23
  * Use junit4
  * Refresh patches for new upstream version
  * Freshen years in debian/copyright
  * Bump Standards-Version to 4.6.1

 -- tony mancill <tmancill@debian.org>  Mon, 13 Jun 2022 21:05:20 -0700

bridge-method-injector (1.18-3) unstable; urgency=medium

  * Team upload.
  * Update Homepage and Source URLs (Closes: #975254)
  * Update version in debian/watch
  * Update to debhelper compat level 13
  * Bump Standards-Version to 4.5.1
  * Set "Rules-Requires-Root: no" in debian/control

 -- tony mancill <tmancill@debian.org>  Sat, 21 Nov 2020 08:33:49 -0800

bridge-method-injector (1.18-2) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 9 (Closes: #893101)
  * Standards-Version updated to 4.1.4
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 06 Jun 2018 15:30:34 +0200

bridge-method-injector (1.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Removed the unused build dependency on libmaven-install-plugin-java
  * Standards-Version updated to 4.1.3

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 12 Jan 2018 10:28:18 +0100

bridge-method-injector (1.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Depend on libmaven3-core-java instead of libmaven2-core-java
  * Removed the unused dependency on libmaven-scm-java
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 31 Jul 2017 01:12:40 +0200

bridge-method-injector (1.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Depend on libasm-java (>= 5.0) instead of libasm4-java
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 3.9.8
  * Use a secure Vcs-Git URL
  * debian/watch: No longer use githubredir.debian.net

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 28 Sep 2016 00:08:23 +0200

bridge-method-injector (1.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Depend on libasm4-java instead of libasm3-java
  * debian/maven.rules: Fixed a typo causing a build failure (Closes: #761540)
  * Switch to debhelper level 9
  * debian/watch: Updated to match the versions >= 1.10
  * Use canonical URLs for the Vcs-* fields

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 15 Sep 2014 08:47:24 +0200

bridge-method-injector (1.8-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Bumped Standard-Version, no changes.
  * d/control: Drop DM-Upload-Allowed field.

 -- James Page <james.page@ubuntu.com>  Sun, 22 Dec 2013 20:34:09 +0000

bridge-method-injector (1.4-3) unstable; urgency=low

  [ James Page ]
  * Use new version of annotation-indexer:
    - d/maven.rules: Map to new org.jenkins-ci groupID.
    - d/control: Added version to BDI to ensure compatibility.
  * Bumped Standards-Version to 3.9.3:
    - d/copyright: Use new released version of DEP-5.
  * d/libbridge-method-injector-java.poms: Use --java-lib option to
    ensure that jar files are installed to /usr/share/java.

  [ tony mancill ]
  * Set DMUA flag.

 -- James Page <james.page@ubuntu.com>  Mon, 30 Apr 2012 14:17:38 +0100

bridge-method-injector (1.4-2) unstable; urgency=low

  * Fix FTBFS with asm3 >= 3.3 (LP: #851659):
    - d/maven.rules: Use asm-all instead of asm to align to restructure
      of jar files.
  * Fix FTBFS due to changes to maven-compiler-plugin (Closes: #643495):
    - d/control: Added libmaven-plugin-tools-java to Build-Deps-Indep.
  * debian/copyright: Tidied new lintian warnings.

 -- James Page <james.page@ubuntu.com>  Wed, 28 Sep 2011 09:46:23 +0100

bridge-method-injector (1.4-1) unstable; urgency=low

  * Initial Debian release (Closes: #629814)

 -- James Page <james.page@ubuntu.com>  Mon, 01 Aug 2011 10:12:58 +0100

bridge-method-injector (1.4-0ubuntu1) oneiric; urgency=low

  * Initial release

 -- James Page <james.page@canonical.com>  Wed, 08 Jun 2011 16:29:57 +0100
